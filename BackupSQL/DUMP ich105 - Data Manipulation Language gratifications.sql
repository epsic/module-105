
/****** Object:  Table [dbo].[tblEmployesArchives]    Script Date: 15.02.2019 14:28:53 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblEmployesArchives](
	[ENo] [smallint] NOT NULL,
	[ENom] [nvarchar](20) NOT NULL,
	[EJob] [nvarchar](20) NOT NULL,
	[EChef] [smallint] NULL,
	[EDebut] [datetime] NULL,
	[Esal] [money] NULL,
	[ECom] [money] NULL,
	[DNo] [smallint] NULL,
 CONSTRAINT [PKtblEmployesArchives] PRIMARY KEY CLUSTERED 
(
	[ENo] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
/****** Object:  Table [dbo].[tblGratifications]    Script Date: 15.02.2019 14:28:54 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[tblGratifications](
	[Eno] [int] NOT NULL,
	[Annee] [int] NOT NULL,
	[Gratification] [money] NULL,
 CONSTRAINT [PKtblGratifications] PRIMARY KEY CLUSTERED 
(
	[Eno] ASC,
	[Annee] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]

GO
INSERT [dbo].[tblEmployesArchives] ([ENo], [ENom], [EJob], [EChef], [EDebut], [Esal], [ECom], [DNo]) VALUES (7997, N'Kottak', N'vendeur', 7902, CAST(0x000063DF00000000 AS DateTime), 1833.0000, NULL, 30)
GO
INSERT [dbo].[tblEmployesArchives] ([ENo], [ENom], [EJob], [EChef], [EDebut], [Esal], [ECom], [DNo]) VALUES (7998, N'Klauss', N'Ouvrier', 7839, CAST(0x0000559A00000000 AS DateTime), 1650.0000, NULL, 10)
GO
INSERT [dbo].[tblEmployesArchives] ([ENo], [ENom], [EJob], [EChef], [EDebut], [Esal], [ECom], [DNo]) VALUES (7999, N'Kurt', N'Ouvirer', 7839, CAST(0x0000475600000000 AS DateTime), 1500.0000, NULL, 20)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7028, 2000, 3000.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7521, 2001, 1500.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7698, 2000, 2000.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7839, 2000, 450.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7876, 2000, 350.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7900, 2000, 1250.0000)
GO
INSERT [dbo].[tblGratifications] ([Eno], [Annee], [Gratification]) VALUES (7902, 2001, 1800.0000)
GO
select * from tblEmployesArchives

select * from tblGratifications