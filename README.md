# Module 105

## Requirement
 - Docker
 - Docker compose
 
## 1. Clone project 
    git clone https://gitlab.com/epsic/module-105.git

## Start Microsoft SQL Server
1. Go to your project location
2. Run command `docker-compose up`

## Connect to database
Using your db tool connect to the database with the following credentials  
Host: localhost  
Port: 1433  
User: sa  
Password: Picaro1234  

## Restore Database
1. Connect to your database
3. Open a console (where you can type SQL commands)
2. Run SQL in the file [restoreDB](restoreDB.sql) one by one

Now you can access your database:
![Database](images/Database.png)
