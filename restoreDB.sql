RESTORE DATABASE ich105DML
    FROM DISK = '/var/opt/mssql/backup/ich105DML.bak'
    WITH MOVE 'ich105-employe' TO '/var/opt/mssql/data/ich105-employe.mdf',
         MOVE 'ich105-employe_log' TO '/var/opt/mssql/data/ich105-employe_log.ldf';
